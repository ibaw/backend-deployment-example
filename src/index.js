import express from 'express'

const app = express()

app.get('/', (req,res) => {
    res.json({message: 'Docker is easy'})
})

const port = process.env.PORT || 3030;
app.listen(port, () => console.log(`app listening on port ${port}`))
